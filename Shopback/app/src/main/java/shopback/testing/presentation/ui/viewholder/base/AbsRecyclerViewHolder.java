package shopback.testing.presentation.ui.viewholder.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;
import shopback.testing.data.model.ui.base.AbsItemData;

/**
 * Created by ThuNguyen on 11/15/15.
 */
public abstract class AbsRecyclerViewHolder<T extends AbsItemData> extends RecyclerView.ViewHolder {
    public interface IItemClickListener<T>{
        void onItemClickListener(T itemData);
    }
    IItemClickListener<T> itemClickListener;
    T itemData;

    protected AbsRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(itemClickListener != null){
                    itemClickListener.onItemClickListener(itemData);
                }
            }
        });
    }

    public void bind(T itemData,int pos){
        this.itemData = itemData;
    }

    public T getDataItem() {
        return null;
    }

    public void setDataItem(T dataItem){
    }

    public void setItemClickListener(IItemClickListener<T> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
