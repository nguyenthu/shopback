package shopback.testing.domain.ws.response.base;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public abstract class AbsWSResponse<T> implements IWSResponse {
    public abstract T getBodyResult();
}
