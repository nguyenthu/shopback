package shopback.testing.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import shopback.testing.R;


public class DialogUtil {

    public static void showInformDialog(Context context, String title, String message, String okLabel, DialogInterface.OnClickListener okListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
        if (title != null && title.length() != 0)
            builder.setTitle(title);


        builder.setMessage(message)
                .setCancelable(true)
                .setPositiveButton(okLabel, okListener)
                .show();
    }
    public static void showInformDialog(Context context, String message, DialogInterface.OnClickListener okListener){
        showInformDialog(context, "", message, "OK", okListener);
    }

    /**
     * Function to showPopup settings alert dialog
     * */
}
