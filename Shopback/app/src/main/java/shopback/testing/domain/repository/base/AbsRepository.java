package shopback.testing.domain.repository.base;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public abstract class AbsRepository {
    protected <T> Observable<T> processResponse(Call<T> request){
        return Observable.create(e -> {
            Response<T> response = request.execute();
            T bodyResponse = response.body();
            if(response.isSuccessful() &&
                    bodyResponse != null){
                e.onNext(bodyResponse);
                e.onComplete();
            }else{
                e.onError(new Exception());
            }
        });
    }
}
