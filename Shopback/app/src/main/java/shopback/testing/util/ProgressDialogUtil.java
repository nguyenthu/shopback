package shopback.testing.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import shopback.testing.R;

/**
 * Created by ThuNguyen on 8/21/2018.
 */
public class ProgressDialogUtil {
    private static ProgressDialog mProgressDialog;
    private static boolean isLoading = false;

    public static void showLoading(Context context, String title) {
        try {
            if (!isLoading) {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                mProgressDialog = new ProgressDialog(context, R.style.ProgressDialogTheme);
                mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        isLoading = false;
                    }
                });
                mProgressDialog.setMessage(title);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();

                isLoading = true;
            }
        } catch (Exception e) {
        }
    }
    public static void hideLoading() {
        if (mProgressDialog != null && isLoading) {
            mProgressDialog.hide();
            mProgressDialog.dismiss();
            isLoading = false;
        }
    }
}
