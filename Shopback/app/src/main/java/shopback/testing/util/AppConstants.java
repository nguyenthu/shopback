package shopback.testing.util;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class AppConstants {
    public static final int PER_PAGE = 20;

    public static final String USER_ITEM_KEY = "user_item_key";
}
