package shopback.testing.domain.interactor.user;

import io.reactivex.Observable;
import shopback.testing.data.model.ws.UserWSData;
import shopback.testing.domain.interactor.base.AbsUseCase;
import shopback.testing.domain.repository.UserRepositoryImpl;
import shopback.testing.domain.repository.base.IUserRepository;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class GetUserDetail extends AbsUseCase<UserWSData, Void, String, Void> {
    IUserRepository repository;

    public GetUserDetail() {
        repository = new UserRepositoryImpl();
    }

    @Override
    public Observable<UserWSData> buildUseCaseSingle() {
        return repository.getUserDetail(pathReqArr[0]);
    }
}
