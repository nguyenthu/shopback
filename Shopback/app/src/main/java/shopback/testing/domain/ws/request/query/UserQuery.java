package shopback.testing.domain.ws.request.query;

import shopback.testing.domain.ws.request.query.base.AbsWSQuery;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserQuery extends AbsWSQuery {
    int since;
    int perPage;

    public UserQuery setSince(int since){
        this.since = since;
        return this;
    }
    public UserQuery setPerPage(int perPage){
        this.perPage = perPage;
        return this;
    }
}
