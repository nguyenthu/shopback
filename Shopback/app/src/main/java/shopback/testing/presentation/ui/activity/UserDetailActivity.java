package shopback.testing.presentation.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import shopback.testing.R;
import shopback.testing.data.model.ui.user.UserDetailItemData;
import shopback.testing.data.model.ui.user.UserItemData;
import shopback.testing.presentation.presenter.IUserPresenter;
import shopback.testing.presentation.presenter.impl.UserPresenterImpl;
import shopback.testing.presentation.ui.activity.base.BaseActivity;
import shopback.testing.util.AppConstants;
import shopback.testing.util.DialogUtil;
import shopback.testing.util.ProgressDialogUtil;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserDetailActivity extends BaseActivity implements IUserPresenter.View{
    @BindView(R.id.ivAvatar)
    ImageView ivUserAvatar;
    @BindView(R.id.tvFullName)
    TextView tvFullName;
    @BindView(R.id.tvBio)
    TextView tvBio;
    @BindView(R.id.tvLogin)
    TextView tvLogin;
    @BindView(R.id.tvSiteAdmin)
    TextView tvSiteAdmin;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.tvBlog)
    TextView tvBlog;

    IUserPresenter.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        ButterKnife.bind(this, this);

        presenter = new UserPresenterImpl(this);

        if(getIntent() != null && getIntent().getExtras() != null){
            // Update UI first
            UserItemData userItemData = getIntent().getExtras().getParcelable(AppConstants.USER_ITEM_KEY);
            if(userItemData != null) {
                updateUI(userItemData);

                // Also load user detail
                ProgressDialogUtil.showLoading(this, getString(R.string.loading_text));
                presenter.getUserDetail(userItemData.getLogin());
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @OnClick(R.id.ibClose)
    public void onClick(){
        finish();
    }

    //////////////////////// Methods for internal process S///////////////////
    /**
     * Update UI for some field from user list
     * @param userItemData
     */
    private void updateUI(UserItemData userItemData){
        if(userItemData != null){
            // Avatar
            Glide.with(this)
                    .load(userItemData.getAvatarUrl())
                    .error(R.mipmap.ic_user_default)
                    .skipMemoryCache(false)
                    .dontTransform()
                    .dontAnimate()
                    .into(ivUserAvatar);

            // User name
            tvLogin.setText(userItemData.getLogin());

            // Admin page
            String displaySiteAdmin = userItemData.getDisplaySiteAdmin();
            if(TextUtils.isEmpty(displaySiteAdmin)){
                tvSiteAdmin.setVisibility(View.GONE);
            }else{
                tvSiteAdmin.setVisibility(View.VISIBLE);
                tvSiteAdmin.setText(displaySiteAdmin);
            }
        }
    }


    /**
     * Update UI for more data
     * @param userDetailItemData
     */
    private void updateUI(UserDetailItemData userDetailItemData){
        if(userDetailItemData != null){
            // Full name
            String fullName = userDetailItemData.getName();
            if(TextUtils.isEmpty(fullName)){
                tvFullName.setVisibility(View.GONE);
            }else{
                tvFullName.setVisibility(View.VISIBLE);
                tvFullName.setText(fullName);
            }

            // Bio
            String bio = userDetailItemData.getBio();
            if(TextUtils.isEmpty(bio)){
                tvBio.setVisibility(View.GONE);
            }else{
                tvBio.setVisibility(View.VISIBLE);
                tvBio.setText(bio);
            }

            // Location
            String location = userDetailItemData.getLocation();
            if(TextUtils.isEmpty(location)){
                tvLocation.setText(R.string.not_available_text);
            }else{
                tvLocation.setText(location);
            }

            // Blog
            String blog = userDetailItemData.getBlog();
            if(TextUtils.isEmpty(blog)){
                tvBlog.setText(R.string.not_available_text);
                tvBlog.setTextColor(Color.BLACK);
            }else{
                tvBlog.setText(blog);
            }
        }
    }
    //////////////////////// Methods for internal process E///////////////////


    //////////////////////// Methods for callback from presenter S///////////////////
    @Override
    public void getUserListResponse(List<UserItemData> userItemDataList, String error) {

    }

    @Override
    public void getUserDetailResponse(UserDetailItemData userItemData, String error) {
        ProgressDialogUtil.hideLoading();
        if(userItemData != null && TextUtils.isEmpty(error)){
            updateUI(userItemData);
        }else{
            DialogUtil.showInformDialog(this, getString(R.string.cannot_get_user_detail_error), null);
        }
    }
    //////////////////////// Methods for callback from presenter E///////////////////
}
