package shopback.testing.data.model.ui.base;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public interface IItemData<P1, P2> {
    P1 castFromWSData(P2 p2);
}
