package shopback.testing.domain.ws.request.query.base;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import shopback.testing.util.RemoveNullStringFactory;

/**
 * Created by Thu Nguyen on 8/22/2017.
 */

public abstract class AbsWSQuery implements IWSQuery {
    transient Gson gson;
    public AbsWSQuery(){
        gson = new GsonBuilder()
                .registerTypeAdapterFactory(new RemoveNullStringFactory())
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }
    @Override
    public Map<String, String> getMap() {
        Map<String, String> map = new HashMap<>();
        JsonElement jsonElement = gson.toJsonTree(this);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        for (Map.Entry<String, JsonElement> e : jsonObject.entrySet()) {
            map.put(e.getKey(), e.getValue().getAsString());
        }
        return map;
    }
}
