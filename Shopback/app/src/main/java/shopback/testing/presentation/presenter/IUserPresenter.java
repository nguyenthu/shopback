package shopback.testing.presentation.presenter;

import java.util.List;

import shopback.testing.data.model.ui.user.UserDetailItemData;
import shopback.testing.data.model.ui.user.UserItemData;
import shopback.testing.presentation.presenter.base.IVP;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public interface IUserPresenter {
    interface View extends IVP.View {
        void getUserListResponse(List<UserItemData> userItemDataList, String error);
        void getUserDetailResponse(UserDetailItemData userItemData, String error);
    }
    interface Presenter extends IVP.Presenter{
        void getUserList(int since, int perPage);
        void getUserDetail(String userName);
    }
}
