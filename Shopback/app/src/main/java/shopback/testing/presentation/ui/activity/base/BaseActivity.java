package shopback.testing.presentation.ui.activity.base;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {
}
