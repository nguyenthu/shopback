package shopback.testing.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import shopback.testing.R;
import shopback.testing.data.model.ui.user.UserDetailItemData;
import shopback.testing.data.model.ui.user.UserItemData;
import shopback.testing.presentation.presenter.IUserPresenter;
import shopback.testing.presentation.presenter.impl.UserPresenterImpl;
import shopback.testing.presentation.ui.activity.base.BaseActivity;
import shopback.testing.presentation.ui.adapter.UserListAdapter;
import shopback.testing.presentation.ui.viewholder.base.AbsRecyclerViewHolder;
import shopback.testing.presentation.ui.widget.RecyclerViewLoadMore;
import shopback.testing.util.AppConstants;
import shopback.testing.util.ProgressDialogUtil;

public class UserListActivity extends BaseActivity implements IUserPresenter.View {
    @BindView(R.id.rvUser)
    RecyclerViewLoadMore rvUserList;
    @BindView(R.id.tvNoUser)
    TextView tvNoUser;


    UserListAdapter userListAdapter;
    IUserPresenter.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this, this);

        setTitle(R.string.github_users_text);

        rvUserList.setOnLoadMoreListener(() ->{
                    getUserList();
                    userListAdapter.loadMore();
                }
            );

        // Call user list at first
        ProgressDialogUtil.showLoading(this, getString(R.string.loading_text));
        presenter = new UserPresenterImpl(this);
        getUserList();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    //////////////////////// Methods for internal process S///////////////////
    private void getUserList(){
        UserItemData lastItemData = userListAdapter != null ? userListAdapter.getLastItem() : null;
        if(lastItemData != null){
            presenter.getUserList(lastItemData.getId(), AppConstants.PER_PAGE);
        }else{
            presenter.getUserList(0, AppConstants.PER_PAGE);
        }
    }
    private void validateUI(boolean isError){
        rvUserList.setVisibility(isError ? View.GONE : View.VISIBLE);
        tvNoUser.setVisibility(isError ? View.VISIBLE : View.GONE);

    }
    //////////////////////// Methods for internal process E///////////////////


    //////////////////////// Methods for callback from presenter S///////////////////
    @Override
    public void getUserListResponse(List<UserItemData> userItemDataList, String error) {
        ProgressDialogUtil.hideLoading();
        if(TextUtils.isEmpty(error)){
            if(userListAdapter == null) {
                userListAdapter = new UserListAdapter(new AbsRecyclerViewHolder.IItemClickListener<UserItemData>() {
                    @Override
                    public void onItemClickListener(UserItemData itemData) {
                        // Go to user detail screen
                        Intent intent = new Intent(UserListActivity.this, UserDetailActivity.class);
                        intent.putExtra(AppConstants.USER_ITEM_KEY, itemData);
                        startActivity(intent);
                    }
                });
                userListAdapter.setDataList(userItemDataList);
                rvUserList.setAdapter(userListAdapter);
            }else{
                rvUserList.onLoadMoreComplete();
                userListAdapter.removeLoadMore();

                userListAdapter.addItemsOnLast(userItemDataList);
                userListAdapter.notifyDataSetChanged();

            }
            // Update data reach to end flag
            rvUserList.setReadEnd(userItemDataList != null && AppConstants.PER_PAGE > userItemDataList.size());
        }

        // Validate UI by error
        if(userListAdapter == null){
            validateUI(!TextUtils.isEmpty(error));
        }
    }

    @Override
    public void getUserDetailResponse(UserDetailItemData userItemData, String error) {

    }
    //////////////////////// Methods for callback from presenter E///////////////////
}
