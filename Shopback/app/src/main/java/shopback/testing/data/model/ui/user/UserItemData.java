package shopback.testing.data.model.ui.user;

import android.os.Parcel;
import android.os.Parcelable;

import shopback.testing.MainApplication;
import shopback.testing.R;
import shopback.testing.data.model.ui.base.AbsItemData;
import shopback.testing.data.model.ui.base.IItemData;
import shopback.testing.data.model.ws.UserWSData;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserItemData extends AbsItemData<UserWSData> implements IItemData<UserItemData, UserWSData>, Parcelable {
    int id;
    String login;
    String avatarUrl;
    boolean siteAdmin;
    public UserItemData(){}

    public UserItemData(UserWSData userWSData) {
        super(userWSData);
        castFromWSData(userWSData);
    }

    protected UserItemData(Parcel in) {
        id = in.readInt();
        login = in.readString();
        avatarUrl = in.readString();
        siteAdmin = in.readByte() != 0;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(login);
        parcel.writeString(avatarUrl);
        parcel.writeByte((byte) (siteAdmin ? 1 : 0));
    }
    public static final Creator<UserItemData> CREATOR = new Creator<UserItemData>() {
        @Override
        public UserItemData createFromParcel(Parcel in) {
            return new UserItemData(in);
        }

        @Override
        public UserItemData[] newArray(int size) {
            return new UserItemData[size];
        }
    };

    @Override
    public UserItemData castFromWSData(UserWSData userWSData) {
        if(userWSData != null){
            id = userWSData.getId();
            login = userWSData.getLogin();
            avatarUrl = userWSData.getAvatarUrl();
            siteAdmin = userWSData.isSiteAdmin();
        }
        return this;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public boolean isSiteAdmin() {
        return siteAdmin;
    }

    public String getDisplaySiteAdmin(){
        if(siteAdmin){
            return MainApplication.getInstance().getString(R.string.staff_text);
        }
        return null;
    }
}
