package shopback.testing.domain.repository.base;

import java.util.List;

import io.reactivex.Observable;
import shopback.testing.data.model.ws.UserWSData;
import shopback.testing.domain.ws.request.query.UserQuery;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public interface IUserRepository extends IRepository {
    Observable<List<UserWSData>> getUserList(UserQuery userQuery);
    Observable<UserWSData> getUserDetail(String userName);
}
