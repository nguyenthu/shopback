package shopback.testing.domain.ws.response.user;

import shopback.testing.data.model.ws.UserWSData;
import shopback.testing.domain.ws.response.base.AbsWSResponse;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserDetailResponse extends AbsWSResponse<UserWSData> {
    @Override
    public boolean isSuccessful() {
        return false;
    }

    @Override
    public UserWSData getBodyResult() {
        return null;
    }
}
