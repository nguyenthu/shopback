package shopback.testing.domain.ws.response.user;

import java.util.List;

import shopback.testing.data.model.ws.UserWSData;
import shopback.testing.domain.ws.response.base.AbsWSResponse;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserListResponse extends AbsWSResponse<List<UserWSData>> {
    @Override
    public boolean isSuccessful() {
        return false;
    }

    @Override
    public List<UserWSData> getBodyResult() {
        return null;
    }
}
