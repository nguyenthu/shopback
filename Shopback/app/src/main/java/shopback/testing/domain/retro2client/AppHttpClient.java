package shopback.testing.domain.retro2client;

import shopback.testing.BuildConfig;
import shopback.testing.domain.ws.restapi.UserApi;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppHttpClient extends Retro2Client {
    UserApi userApi;

    private static class AppHttpClientHelper {
        private static final AppHttpClient INSTANCE = new AppHttpClient();
    }

    public static AppHttpClient getInstance() {
        return AppHttpClientHelper.INSTANCE;
    }

    public UserApi getUserApi(){
        return userApi;
    }
    private AppHttpClient() {
        final Retrofit rootRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.WS_ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(provideOkHttpClient())
                .build();
        userApi = rootRetrofit.create(UserApi.class);
    }
}
