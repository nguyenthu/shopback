package shopback.testing;

import android.app.Application;

import io.reactivex.internal.functions.Functions;
import io.reactivex.plugins.RxJavaPlugins;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class MainApplication extends Application {
    private static MainApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        RxJavaPlugins.setErrorHandler(Functions.<Throwable>emptyConsumer());
    }
    public static Application getInstance(){
        return instance;
    }
}
