package shopback.testing.presentation.ui.adapter.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import shopback.testing.data.model.ui.base.AbsItemData;
import shopback.testing.presentation.ui.viewholder.base.AbsRecyclerViewHolder;

/**
 * Created by ThuNguyen on 8/21/2018.
 */

public abstract class AbsRecyclerViewAdapter <T extends AbsItemData, U extends AbsRecyclerViewHolder> extends RecyclerView.Adapter<U>{

    protected Context context;
    public static final int VIEW_ITEM = 1;
    public static final int VIEW_PROG = 0;
    public static final int VIEW_HEADER = 2;
    public static final int VIEW_FOOTER = 3;
    List<T> dataList;

    public T getItem(int position) {
        return dataList.get(position);
    }

    public void setDataList(List<T> dataList){
        this.dataList = dataList;
    }

    public List<T> getDataList(){
        return dataList;
    }

    /**
     * Add the items to the current list on last
     * @param dataList
     */
    public void addItemsOnLast(List<T> dataList){
        if(dataList != null && dataList.size() > 0){
            this.dataList.addAll(dataList);
        }
    }

    /**
     * Add the items to the current list on first
     * @param dataList
     */
    public void addItemsOnFirst(List<T> dataList){
        if(dataList != null && dataList.size() > 0){
            this.dataList.addAll(0, dataList);
        }
    }
    public void loadMore(){
        dataList.add(null);
        notifyDataSetChanged();
    }
    public void removeLoadMore(){
        dataList.remove(null);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(dataList.get(position) != null){
            return VIEW_ITEM;
        }
        return VIEW_FOOTER;
    }

    public T getLastItem(){
        if(dataList != null && dataList.size() > 0){
            return dataList.get(dataList.size() - 1);
        }
        return null;
    }
}
