package shopback.testing.presentation.ui.viewholder;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import shopback.testing.R;
import shopback.testing.data.model.ui.user.UserItemData;
import shopback.testing.presentation.ui.viewholder.base.AbsRecyclerViewHolder;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserListViewHolder extends AbsRecyclerViewHolder<UserItemData>{
    @BindView(R.id.ivAvatar)
    ImageView ivUserAvatar;
    @BindView(R.id.tvLogin)
    TextView tvLogin;
    @BindView(R.id.tvSiteAdmin)
    TextView tvSiteAdmin;

    public UserListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(UserItemData userItemData, int pos) {
        super.bind(userItemData, pos);

        // Render UI
        Glide.with(itemView.getContext())
                .load(userItemData.getAvatarUrl())
                .error(R.mipmap.ic_user_default)
                .skipMemoryCache(false)
                .dontTransform()
                .dontAnimate()
                .into(ivUserAvatar);

        tvLogin.setText(userItemData.getLogin());

        String displaySiteAdmin = userItemData.getDisplaySiteAdmin();
        if(TextUtils.isEmpty(displaySiteAdmin)){
            tvSiteAdmin.setVisibility(View.GONE);
        }else{
            tvSiteAdmin.setVisibility(View.VISIBLE);
            tvSiteAdmin.setText(displaySiteAdmin);
        }
    }
}
