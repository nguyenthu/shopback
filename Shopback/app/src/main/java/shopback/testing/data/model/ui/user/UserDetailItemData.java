package shopback.testing.data.model.ui.user;

import shopback.testing.data.model.ws.UserWSData;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserDetailItemData extends UserItemData {
    String name;
    String bio;
    String location;
    String blog;

    public UserDetailItemData(UserWSData userWSData) {
        super(userWSData);

        // Render some more info
        name = userWSData.getName();
        bio = userWSData.getBio();
        location = userWSData.getLocation();
        blog = userWSData.getBlog();
    }

    public String getName() {
        return name;
    }

    public String getBio() {
        return bio;
    }

    public String getLocation() {
        return location;
    }

    public String getBlog() {
        return blog;
    }
}
