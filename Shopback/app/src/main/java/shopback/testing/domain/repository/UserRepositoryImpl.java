package shopback.testing.domain.repository;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import shopback.testing.data.model.ws.UserWSData;
import shopback.testing.domain.repository.base.AbsRepository;
import shopback.testing.domain.repository.base.IUserRepository;
import shopback.testing.domain.retro2client.AppHttpClient;
import shopback.testing.domain.ws.request.query.UserQuery;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserRepositoryImpl extends AbsRepository implements IUserRepository {
    @Override
    public Observable<List<UserWSData>> getUserList(UserQuery userQuery) {
        Call<List<UserWSData>> request = AppHttpClient.getInstance().getUserApi()
                .getUserList(userQuery.getMap());
        return processResponse(request);
    }

    @Override
    public Observable<UserWSData> getUserDetail(String userName) {
        Call<UserWSData> request = AppHttpClient.getInstance().getUserApi()
                .getUserDetail(userName);
        return processResponse(request);
    }
}
