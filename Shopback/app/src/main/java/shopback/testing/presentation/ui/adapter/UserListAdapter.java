package shopback.testing.presentation.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import shopback.testing.R;
import shopback.testing.data.model.ui.user.UserItemData;
import shopback.testing.presentation.ui.adapter.base.AbsRecyclerViewAdapter;
import shopback.testing.presentation.ui.viewholder.LoadmoreProgressBarViewHolder;
import shopback.testing.presentation.ui.viewholder.UserListViewHolder;
import shopback.testing.presentation.ui.viewholder.base.AbsRecyclerViewHolder;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserListAdapter extends AbsRecyclerViewAdapter<UserItemData, AbsRecyclerViewHolder>{
    AbsRecyclerViewHolder.IItemClickListener<UserItemData> itemDataIItemClickListener;

    public UserListAdapter(){

    }
    public UserListAdapter(AbsRecyclerViewHolder.IItemClickListener<UserItemData> itemDataIItemClickListener){
        this.itemDataIItemClickListener = itemDataIItemClickListener;
    }
    @Override
    public AbsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_ITEM) {
            UserListViewHolder userListViewHolder = new UserListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.ctrl_user_item, parent, false));
            userListViewHolder.setItemClickListener(itemDataIItemClickListener);
            return userListViewHolder;
        }
        return new LoadmoreProgressBarViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.ctrl_loadmore_progressbar, parent, false));
    }

    @Override
    public void onBindViewHolder(AbsRecyclerViewHolder holder, int position) {
        holder.bind(getItem(position), position);
    }
}
