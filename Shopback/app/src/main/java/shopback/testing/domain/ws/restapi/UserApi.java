package shopback.testing.domain.ws.restapi;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import shopback.testing.data.model.ws.UserWSData;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public interface UserApi {
    @GET("users")
    Call<List<UserWSData>> getUserList(@QueryMap Map<String, String> query);

    @GET("users/{user_name}")
    Call<UserWSData> getUserDetail(@Path("user_name") String userName);
}
