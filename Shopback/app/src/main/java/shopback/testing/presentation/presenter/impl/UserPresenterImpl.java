package shopback.testing.presentation.presenter.impl;

import shopback.testing.data.model.ui.user.UserDetailItemData;
import shopback.testing.data.model.ui.user.UserItemData;
import shopback.testing.domain.interactor.user.GetUserDetail;
import shopback.testing.domain.interactor.user.GetUserList;
import shopback.testing.domain.ws.request.query.UserQuery;
import shopback.testing.presentation.presenter.IUserPresenter;
import shopback.testing.presentation.presenter.base.BasePresenter;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class UserPresenterImpl extends BasePresenter<IUserPresenter.View> implements IUserPresenter.Presenter {
    GetUserDetail getUserDetail;
    GetUserList getUserList;

    public UserPresenterImpl(IUserPresenter.View view) {
        super(view);
        getUserList = new GetUserList();
        getUserDetail = new GetUserDetail();
    }

    @Override
    public void getUserList(int since, int perPage) {
        getUserList.query(new UserQuery().setSince(since).setPerPage(perPage));
        Disposable disposable = getUserList.buildUseCaseSingle()
                .flatMap(userWSDataList ->
                        Observable.fromIterable(userWSDataList)
                                .map(item -> new UserItemData(item))
                                .toList()
                                .toObservable() // Required for RxJava 2.x
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userItemDataList -> view.getUserListResponse(userItemDataList, null),
                        throwable -> view.getUserListResponse(null, throwable.getMessage())
                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void getUserDetail(String userName) {
        getUserDetail.path(userName);
        Disposable disposable = getUserDetail.buildUseCaseSingle()
                .flatMap(userWSData ->
                        Observable.just(new UserDetailItemData(userWSData))// Required for RxJava 2.x
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userDetailItemData -> view.getUserDetailResponse(userDetailItemData, null),
                        throwable -> view.getUserDetailResponse(null, throwable.getMessage())
                );
        compositeDisposable.add(disposable);
    }
}
