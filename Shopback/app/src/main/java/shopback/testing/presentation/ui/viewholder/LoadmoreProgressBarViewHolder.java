package shopback.testing.presentation.ui.viewholder;

import android.view.View;
import android.widget.ProgressBar;

import shopback.testing.R;
import shopback.testing.presentation.ui.viewholder.base.AbsRecyclerViewHolder;

/**
 * Created by ThuNguyen on 11/15/15.
 */
public class LoadmoreProgressBarViewHolder extends AbsRecyclerViewHolder {
    public ProgressBar progressBar;

    public LoadmoreProgressBarViewHolder(View v) {
        super(v);
        progressBar = (ProgressBar) v.findViewById(R.id.loadMorProgressBar);
    }
}
