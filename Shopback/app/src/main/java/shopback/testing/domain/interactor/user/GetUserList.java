package shopback.testing.domain.interactor.user;

import java.util.List;

import io.reactivex.Observable;
import shopback.testing.data.model.ws.UserWSData;
import shopback.testing.domain.interactor.base.AbsUseCase;
import shopback.testing.domain.repository.UserRepositoryImpl;
import shopback.testing.domain.repository.base.IUserRepository;
import shopback.testing.domain.ws.request.query.UserQuery;

/**
 * Created by Thu Nguyen on 8/21/2018.
 */

public class GetUserList extends AbsUseCase<List<UserWSData>, UserQuery, Void, Void> {
    IUserRepository repository;

    public GetUserList() {
        repository = new UserRepositoryImpl();
    }

    @Override
    public Observable<List<UserWSData>> buildUseCaseSingle() {
        return repository.getUserList(queryReq);
    }
}
